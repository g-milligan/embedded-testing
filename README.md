# Embedded Testing

These files are the starting point for a half or full day of embedded testing workshop.  To use this project:

1. Log in to your [GitLab](https://gitlab.com) account.  Create a new account if you don't have one.

1. Go to the [Embedded Testing](https://gitlab.com/ClayDowling/embedded-testing) repository.

1. **FORK THIS REPOSITORY**  You don't have commit rights to my repository, so you need your own copy.

1. Open the WebIDE.


If you were wondering what my typical work day looks like, this is my cat supervising the creation of this repository.

![Thea is "Helping"](thea-helping-with-code.jpg)
