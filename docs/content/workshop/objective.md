---
title: "Objective"
date: 2019-01-01T16:41:23-05:00
---

The Objective of the workshop is to test drive Blink, the ubiquitous "Hello World" program of the embedded world.

## Requirements

1. Pin 13, which drives the built in LED, should be enabled for output.
1. The built in led should be driven high.
1. After a one second delay, pin 13 should be driven low.
1. After a one second delay, pin 13 should be driven high again.
1. This cycle should continue as long as the system has power.

## Implementation Details

You can implement the solution using the [Arduino library](http://arduino.cc).  If you are new to embedded development, this will be the easiest route.  You will gain experience mocking functions and headers from an external library.  A good example of a working implementation can be seen at [Arduino - Blink](https://github.com/ThrowTheSwitch/arduino_c_scaffold).

You can also implement the solution using [avrlibc](https://www.nongnu.org/avr-libc/), the low level library which Arduino uses.  You will gain experience mocking and testing system registers, as well as functions from an external library.  A good example of a working implementation can be seen at [Throw the Switch](https://github.com/ThrowTheSwitch/arduino_c_scaffold).