---
title: "Test Doubles"
date: 2019-01-01T11:23:11-05:00
---

A test double is a bit like a stunt double in a movie.  They stand in for functions and objects that we can't use in our test.

## Types of Doubles

* Stub - a function you can call, and will take no action.  If the function is expected to return a value, it will return a fixed value.
* Mock - a function which can remember the parameters it was called with, can return varying vaues based on input, and generally has configurable behavior.
* Fake - a simplified version of the real object.  You might use a local storage emulator for a cloud storage service, or an in-memory database in place of a database server.

In our tests we will create many mocks for functions in the Arduino library which we don't want to actually call while running our tests.  The actual functions reference hardware we don't have on our workstations.