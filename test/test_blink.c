#include "Arduino.h"
#include "blink.h"
#include "mock_pinmode.h"


#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP(Blink);

TEST_SETUP(Blink) {}

TEST_TEAR_DOWN(Blink) {}

TEST(Blink, Setup_byDefault_SetsPinmodeToOutput) {
  blink_setup();

  // Query pinMode mock to parameters
  TEST_ASSERT_TRUE(pinMode_called_with(LED_BUILTIN, OUTPUT));
}